[![Gitter](https://badges.gitter.im/DroidGame/DroidGame3D.svg)](https://gitter.im/DroidGame/DroidGame3D?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge) 

[на русском](https://gitlab.com/polskiychel/droidgame3d/-/blob/main/README_RUS_.md)
[itch.io page](https://ma3rx.itch.io/droid-game-3d)

![image](https://gitlab.com/polskiychel/droidgame3d/-/raw/main/logo.png)

# Droid Game 3D
This is a very interesting game in which you can fight droid bots and online with other players. Unite in teams, and arrange a capture of the flag.<br><br>

# Some information about the game

At the moment the game is on release 1.0, snapshot N3. 

<h3>Popular servers</h3> <br><br>

At the moment, the most popular are the admin server and DOOMPY.<br><br>

<h3>Server statistics</h3> <br><br>

Admin fun server - 184 players per day.<br>
DOOMPY - 47 players per day.<br>
# Features
✓Сapture the flag mode.<br>
✘New bots<br>
✘New droids.<br>
# Gameplay
<h3>Control</h3>
There are many different features in the game at the moment. You can study them all as follows:<br><br>

1.  Download the game<br>
2.  Unpack the archive as you like<br>
3.  Enter six commands for installing modules in the terminal:<br>
<code>pip install panda3d</code><br>
<code>pip install screeninfo</code><br>
<code>pip install ipgetter2</code><br>
<code>pip install PySimpleGUI</code><br>
<code>pip install simplepbr</code><br>
<code>pip install pyperclip</code><br>
<code>pip install Cython</code><br>
4.  Open the directory with the game<br>
5.  Find the main.py file<br>
6.  Run it<br>
7.  In the menu, click on help.<br>
Learn the controls and play!<br>

# Some screenshots
***Menu screenshots :***

Main menu :
![image](https://imgur.com/wcmLLc7.png)
Moderators menu :
![image](https://imgur.com/O1ZUTWr.png)
Developers menu :
![image](https://imgur.com/tCJVwdM.png)
Top players menu :
![image](https://imgur.com/JYbM2H5.png)
Tutorial menu :
![image](https://imgur.com/OkVvz4N.png)
Connect server menu :
![image](https://imgur.com/t8ksyv5.png)
Chat menu :
![image](https://imgur.com/YOgPWMT.png)
Choose droid menu : 
![image](https://imgur.com/JZFhjsC.png)
<h3>In game screenshot</h3>

![image](https://imgur.com/Hy2gQenl.png)

That's screenshot made in online mode. 
This screenshot does not show everything you can do in the game. (Server: DOOMPY)

# Thanks
Special thanks to <a href="https://discourse.panda3d.org/u/panda3dmastercoder"> panda3dmastercoder </a>.
He was the first to help me with development and created this repository.
Thank you <a href="https://discourse.panda3d.org/u/wezu"> wezu </a> for the wonderful DirectGUIDesigner program.
I used it to create a menu.
Thanks also <a href="https://discourse.panda3d.org/u/et1337"> et1337 </a> (<a href="https://github.com/etodd"> ettod </a>) for games a3p.
From there I took the planet model and menu system. Unfortunately, at the moment a3p is going through a difficult time. You can [support](https://github.com/etodd/a3p) this project. Never mind that the last commit was in 2012. Yes, this is a very old game, but you can still play it by downloading it from the [official game site](http://a3p.sf.net/)

# Another sites
<iframe src="https://itch.io/embed/1188941?bg_color=070707&amp;fg_color=327345&amp;link_color=327345&amp;border_color=fb922b" frameborder="0" width="552" height="167"><a href="https://ma3rx.itch.io/droid-game-3d">Droid Game 3D by MA3RX</a></iframe>
