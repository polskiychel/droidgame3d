# C сборка
[original readme](https://gitlab.com/polskiychel/droidgame3d/-/blob/main/c_%20assembly.md)

Вы можете легко собрать игры на язык программирования C, если версия которая есть в директории optimizing устарела, или мы забыли её обновить, просто откройте терминал перейдите в папку с игрой. И напишите в терминал : <code>pip install Cython</code>
После этого ожидайте полной сборки. Но я совсем забыл! Для запуска сборки вам необходим модуль Cython! Введите команду в терминале для установки этого модуля : <code>python setup.py build_ext --inplace</code>
По сути это всё что вам понадобится для сборки игры.
