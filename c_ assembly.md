# C assembly
[на русском](https://gitlab.com/polskiychel/droidgame3d/-/blob/main/c_assembly(RUS).md)

You can easily build games into the C programming language, if the version in the optimizing directory is outdated, or we forgot to update it, just open the terminal and go to the game folder. And write to the terminal:<code>python setup.py build_ext --inplace</code>
After that, expect a complete build. But I completely forgot! You need the Cython module to run the build! Enter the command in terminal to install this module: <code>pip install Cython</code>
In fact, this is all you need to build the game.
