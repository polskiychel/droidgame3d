[![Gitter](https://badges.gitter.im/DroidGame/DroidGame3D.svg)](https://gitter.im/DroidGame/DroidGame3D?utm_source=badge&utm_medium=badge&utm_campaign=pr-badge)

[original readme](https://gitlab.com/polskiychel/droidgame3d/-/blob/main/README.md)
[itch.io страница](https://ma3rx.itch.io/droid-game-3d)

![image](https://gitlab.com/polskiychel/droidgame3d/-/raw/main/logo.png)
# Droid Game 3D
Это очень интересная игра, в которой вы можете сражаться с ботами-дроидами и онлайн с другими игроками. Объединяйтесь в команды и устройте захват флага. <br> <br>

# Немного информации об игре
На данный момент игра на версии релиз 1.0, 3 снапшот.

<h3>Популярные серверы</h3> <br> <br>
На данный момент наиболее популярны админ-сервер и DOOMPY. <br> <br>

<h3>Статистика серверов</h3> <br> <br>

Admin fun server - 184 игрокa в день. <br>
DOOMPY - 47 игроков в день. <br>

# Функции
✓Режим захвата флага. <br>
✘Новые боты <br>
✘Новые дроиды. <br>
# Геймплей
<h3> Управление </h3>
На данный момент в игре много разных функций. 
Вы можете изучить их все следующим образом: <br> <br>

1. Загрузите игру <br>
2. Распаковать архив как угодно <br>
3. Введите шесть команды для установки модулей в терминал: <br>
<code> pip install panda3d </code> <br>
<code> pip install ipgetter2 </code> <br>
<code> pip install PySimpleGUI </code> <br>
<code> pip install simplepbr </code> <br>
<code> pip install pyperclip </code> <br>
<code>pip install Cython</code><br>
4. Откройте каталог с игрой <br>
5. Найдите файл main.py <br>
6. Запустите <br>
7. В меню нажмите «Справка». <br>
Изучите управление и играйте! <br>

# Немного скриншотов
***Скриншоты меню:***

Главное меню :
![image](https://imgur.com/wcmLLc7.png)
Меню модераторов:
![image](https://imgur.com/O1ZUTWr.png)
Меню разработчиков:
![image](https://imgur.com/tCJVwdM.png)
Меню лучших игроков:
![image](https://imgur.com/JYbM2H5.png)
Учебное меню:
![image](https://imgur.com/OkVvz4N.png)
Меню подключения к серверу:
![image](https://imgur.com/t8ksyv5.png)
Меню чата:
![image](https://imgur.com/YOgPWMT.png)
Выберите меню дроидов:
![image](https://imgur.com/JZFhjsC.png)
***Скриншот из игры:***

![image](https://imgur.com/Hy2gQenl.png)

Это скриншот, сделанный в онлайн-режиме.

Этот снимок экрана не показывает все, что вы можете делать в игре. (Сервер: DOOMPY)

# Благодарности
Особая благодарность <a href="https://discourse.panda3d.org/u/panda3dmastercoder"> panda3dmastercoder </a>. 
Он первым начал помогать мне в разработке и создал этот репозиторий. 
Спасибо <a href="https://discourse.panda3d.org/u/wezu"> wezu </a> за замечательную программу DirectGUIDesigner. 
Я использовал его для создания меню. 
Также спасибо <a href="https://discourse.panda3d.org/u/et1337"> et1337 </a> (<a href="https://github.com/etodd"> ettod </a> ) для игрy a3p. 
Оттуда я взял модель планеты и систему меню. К сожалению, на данный момент a3p переживает не лучшее время. Вы можете [поддержать](https://github.com/etodd/a3p) этот проект. Не обращайте внимания, что последний коммит был в 2012 году. Да, это очень старая игра, но вы можете всё равно поиграть в неё скачав на [оффициальном сайте игры](http://a3p.sf.net/)
